define([
	'require'
],
function(require){
	var PushNotification = {
		init : function(callback){
			document.addEventListener('deviceready', function(){PushNotification.onReady(callback);}, false);
		},

		onReady : function(callback){
			if(PushbotsPlugin.isiOS()){
			    PushbotsPlugin.initializeiOS("56587e80177959b12c8b4568");
			}
			else if(PushbotsPlugin.isAndroid()){
			    PushbotsPlugin.initializeAndroid("56587e80177959b12c8b4568", "515968147339");
			}

			PushbotsPlugin.debug(true);

			callback();
		},

		subscribe : function(channel, token, callback){
			if(window.plugins && window.plugins.OrtcPushPlugin){
				var OrtcPushPlugin = window.plugins.OrtcPushPlugin;
				OrtcPushPlugin.log("Connecting");

				OrtcPushPlugin.connect(
					{
						'appkey'	: 'qboSVI',
						'token'		: token,
						'url'		: 'https://ortc-developers.realtime.co/server/ssl/2.1/',
						'projectId' : '515968147339'
					},
					function (e){
						OrtcPushPlugin.log("Connected: ");
						OrtcPushPlugin.log("Trying to subscribe: " + channel);
						OrtcPushPlugin.subscribe(
							{
								channel : channel
							},
							function (evt){
								OrtcPushPlugin.log("subscribed: " + channel);
								if(callback) callback();
							}
						);
					}
				);
			}
		},

		unsubscribe : function(channel, token, callback){
			if(window.plugins && window.plugins.OrtcPushPlugin){
				var doUnsubscribe = function(error){
					if(error) console.log('Error on APNS Subscription', error);
					document.removeEventListener('onException', doUnsubscribe);
					PushNotification.doUnsubscribe(channel, callback);
				};

				document.addEventListener('onException', doUnsubscribe, false);
				PushNotification.subscribe(channel, token, doUnsubscribe);
			}
		},

		doUnsubscribe : function(channel, callback){
			var doCallback = function(error){
				if(error) console.log('Error on APNS Unsubscribe', error);
				document.removeEventListener('onException', doCallback, false);

				OrtcPushPlugin.log("Push Unsubscribed : " + channel);
				if(callback) callback();
			};

			document.addEventListener('onException', doCallback, false);

			var OrtcPushPlugin = window.plugins.OrtcPushPlugin;
				OrtcPushPlugin.log("Push Unsubscribing : " + channel);
				OrtcPushPlugin.unsubscribe({channel : channel}, doCallback);
		},

		disconnect : function(){
			if(window.cordova){
				console.log('Push Disconnecting...');
				PushbotsPlugin.unregister();
				PushNotification.setBadgeNumber(0);
			}
		},

		onReceived : function(notification){
			console.log(notification);
			var OrtcPushPlugin = window.plugins.OrtcPushPlugin;
				OrtcPushPlugin.cancelAllLocalNotifications(function(result){
					console.log('Push Cancelled All Notifs', result);
				});
		},

		setBadgeNumber : function(nb){
			if(window.cordova){
				var OrtcPushPlugin = window.plugins.OrtcPushPlugin;
				OrtcPushPlugin.setApplicationIconBadgeNumber(nb, function(){});
			}
			
		}
	};

	return PushNotification;
});